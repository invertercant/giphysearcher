//
//  MainCoordinator.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 06/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//

import UIKit

class MainCoordinator: NavigationCoordinator {
    
    override func start() {
        let viewModel = SearchScreenViewModel()
        let view = SearchVC()
        view.viewModel = viewModel
        navigationController.pushViewController(view, animated: false)
    }
    
}
