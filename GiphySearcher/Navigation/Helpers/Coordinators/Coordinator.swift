//
//  Coordinator.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 06/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    func start()
}
