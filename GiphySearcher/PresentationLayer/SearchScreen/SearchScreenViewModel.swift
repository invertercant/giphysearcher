//
//  SearchScreenModel.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 06/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//
import RxSwift
import RxCocoa
import RxSwiftExt
import GiphyCoreSDK

enum SearchEventType{
    case nextPage
    case newQuery(query: String)
}

class SearchParametersState{
    var searchOffset: Int = 0
    var query: String = ""
}

class SearchScreenViewModel: SearchScreenViewModelType{
    
    private let bag = DisposeBag()
    let portionSize : Int = 25
    var queryInput: PublishSubject<String> = PublishSubject()
    var scrollInput: PublishSubject<Bool> = PublishSubject()
    private var error: PublishSubject<String> = PublishSubject()
    var errorOutput: Observable<String> {
        return error.asObservable()
    }
    private var loading: PublishSubject<Bool> = PublishSubject()
    var loadingOutput: Observable<Bool>{
        return loading.asObservable()
    }
    var imageCollectionInfo: BehaviorRelay<[GPHImage]> = BehaviorRelay(value: [])
    
    var searchEvent: Observable<SearchEventType> = Observable.empty()
    
    init() {
        
        searchEvent = Observable.from([
            queryInput.map{ text in SearchEventType.newQuery(query: text)},
            scrollInput.map{_ in SearchEventType.nextPage}
            ])
            .merge()
            .share()
        
        let state = searchEvent
            .scan(SearchParametersState()) { accumulator, searchEvent -> SearchParametersState in
                switch searchEvent{
                case .newQuery(let query):
                    accumulator.query = query
                    accumulator.searchOffset = 0
                case .nextPage:
                    accumulator.searchOffset = accumulator.searchOffset + self.portionSize
                }
                return accumulator
        }
        .share()
        
        let imagesPortionInfo = state
            .flatMap{ [weak self] inputs -> Observable<GPHListMediaResponse> in
                guard let strongSelf = self else { return Observable.empty()}
                print("event request. query:\(inputs.query) offset: \(inputs.searchOffset)")
                strongSelf.loading.onNext(true)
                return GiphyCore.shared.rx.search(query: inputs.query, media: .gif, offset: inputs.searchOffset, limit: strongSelf.portionSize){
                        strongSelf.loading.onNext(false)
                    }
                    .catchError({ _ -> Observable<GPHListMediaResponse> in
                        strongSelf.error.onNext("Ошибка связи с сервером")
                        strongSelf.loading.onNext(false)
                        return Observable.empty()
                    })
                
            }
            .map{$0.data}
            .unwrap()
            .map{ (mediaArray) -> [GPHImage] in
                return mediaArray.compactMap{ media -> GPHImage? in
                    return media.images?.fixedWidth
                }
            }
            .share()

        Observable.zip(imagesPortionInfo, state) { (imagesInfo: $0, state: $1) }
            .scan([]) { (accumulator, inputs) -> [GPHImage] in
                let collection = inputs.state.searchOffset == 0 ? inputs.imagesInfo : Array(accumulator + inputs.imagesInfo)
                print("event collection size: \(collection.count)")
                return collection
        }
            .bind(to: imageCollectionInfo)
            .disposed(by: bag)
  
    }
    
}
