//
//  SearchCell.swift
//  GiphySearcher
//
//  Created by Александр Савченко on 09/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//

import UIKit
import Gifu

class SearchCell: UICollectionViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: Gifu.GIFImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
