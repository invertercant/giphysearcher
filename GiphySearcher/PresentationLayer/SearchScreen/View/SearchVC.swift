//
//  SearchVC.swift
//  GiphySearcher
//
//  Created by Александр Савченко on 17/12/2018.
//  Copyright © 2018 Александр Савченко. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Nuke

class SearchVC: UIViewController, SearchScreenViewType{
    
    var bag = DisposeBag()
    var columnWidth : CGFloat {
        var result: CGFloat = 0
        if let layout = collection.collectionViewLayout as? PinterestLayout{
            result = layout.columnWidth
        }
        return result
    }
    let searchDelay: Double = 0.5
    var placeholderColors : [UIColor] = [
        UIColor.orange,
        UIColor.purple,
        UIColor.red,
        UIColor.cyan
    ]
    var viewModel : SearchScreenViewModelType?
    
    var textInput: Observable<String> = Observable.empty()
    var scrollInput: Observable<Bool> = Observable.empty()
    
    
    @IBOutlet weak var collection: UICollectionView!{
        didSet{
            collection.register(UINib(nibName: "SearchCell", bundle: nil), forCellWithReuseIdentifier: "searchCell")
            collection.collectionViewLayout = PinterestLayout(numberOfColumns: 2)
            if let layout = collection.collectionViewLayout as? PinterestLayout{
                layout.delegate = self
            }
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: ((searchTextField.frame.height) * 0.70), height: ((searchTextField.frame.height) * 0.70)))
            activityIndicator.hidesWhenStopped = true
            activityIndicator.color = UIColor.black
            searchTextField.rightView = activityIndicator
            searchTextField.rightViewMode = .always
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let hideOnTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideOnTapGestureRecognizer)
        
        navigationController?.isNavigationBarHidden = true
        setObservables()
        bindViewModel()
        setSubscriptions()
        
    }
    
    func setObservables(){
        
        textInput = searchTextField.rx.text.orEmpty.asObservable()
            .debounce(searchDelay, scheduler: MainScheduler.instance)
            .filter{$0.count > 2}
            .distinctUntilChanged()
            .share()
        
        scrollInput = collection.rx.reachedBottom()
                    .filter{$0}
                    .share()
        
    }
    
    func setSubscriptions(){
        
        guard let viewModel = viewModel else {return}
        
        viewModel.imageCollectionInfo
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self]_ in
                guard let strongSelf = self else {return}
                if let layout = strongSelf.collection.collectionViewLayout as? PinterestLayout{
                    layout.contentHeight = 0
                }
                strongSelf.collection.collectionViewLayout.invalidateLayout()
            })
            .disposed(by: bag)
        
        textInput
            .subscribe(onNext: { [weak self] _ in
                self?.collection.setContentOffset(.zero, animated: false)
            })
            .disposed(by: bag)
        
    }
    
    func bindViewModel(){
        
        guard let viewModel = viewModel else {return}
        
        textInput
            .bind(to: viewModel.queryInput)
            .disposed(by: bag)
        
        scrollInput
            .bind(to: viewModel.scrollInput)
            .disposed(by: bag)
        
        viewModel.imageCollectionInfo
            .map({ (imagesInfoArray) -> [String] in
                return imagesInfoArray.compactMap { imageInfo -> String? in
                    return imageInfo.gifUrl
                }
            })
            .bind(to: collection.rx.items(cellIdentifier: "searchCell", cellType: SearchCell.self)){ [weak self] row, element, cell  in
                guard let url = URL(string: element), let strongSelf = self else {return}
                
                let height = strongSelf.collectionView(strongSelf.collection, heightForPhotoAtIndexPath: IndexPath(item: row, section: 0))
                let width = strongSelf.columnWidth
                let color = strongSelf.placeholderColors[Int.random(in: 0...strongSelf.placeholderColors.count-1)]
                let placeholder = color.image(CGSize(width: width, height: height))
                
                cell.activityIndicator.startAnimating()
                let options = ImageLoadingOptions(placeholder: placeholder,
                                                  transition: .fadeIn(duration: 0.33))
                Nuke.loadImage(with: url, options: options, into: cell.imageView){ _, _ in
                    cell.activityIndicator.stopAnimating()
                }
            }
            .disposed(by: bag)
        
        viewModel.errorOutput
            .observeOn(MainScheduler.instance)
            .bind(onNext: { text in
                self.showError(message: text)
            })
            .disposed(by: bag)
        
        viewModel.loadingOutput
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] isLoading in
                guard let strongSelf = self else {return}
                guard let activityIndicator = strongSelf.searchTextField.rightView as? UIActivityIndicatorView else {return}
                if isLoading {
                    activityIndicator.startAnimating()
                } else {
                    activityIndicator.stopAnimating()
                }
            })
            .disposed(by: bag)
        
       
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
 
}

extension SearchVC: PinterestLayoutDelegate{
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        guard let currentImageInfoCollection = viewModel?.imageCollectionInfo.value else {return CGFloat(100)}
        guard currentImageInfoCollection.count > indexPath.item else {return CGFloat(100)}
        let  scaleFactor = columnWidth/CGFloat(currentImageInfoCollection[indexPath.item].width)
        return CGFloat(currentImageInfoCollection[indexPath.item].height) * scaleFactor
    }
    
}

