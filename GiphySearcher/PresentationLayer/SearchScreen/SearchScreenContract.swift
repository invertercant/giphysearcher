//
//  SearchPresenter.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 06/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//
import RxSwift
import RxCocoa
import GiphyCoreSDK

protocol SearchScreenViewType: CommonViewProtocol{
    
    var viewModel: SearchScreenViewModelType? {get set}
    
}

protocol SearchScreenViewModelType{
    
    var queryInput: PublishSubject<String> {get set}
    var scrollInput: PublishSubject<Bool> {get set}
    var imageCollectionInfo: BehaviorRelay<[GPHImage]> {get set}
    var errorOutput: Observable<String> {get}
    var loadingOutput: Observable<Bool> {get}
    
    
}

