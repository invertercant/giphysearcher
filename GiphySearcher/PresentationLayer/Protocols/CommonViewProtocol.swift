//
//  CommonViewProtocol.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 06/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//

import UIKit

protocol CommonViewProtocol: class {
    
    func set(title: String)
    func showError(message: String)
    
}

extension CommonViewProtocol where Self: UIViewController {
    
    func set(title: String) {
        navigationItem.title = title
    }
    
    func showError(message: String) {
        showErrorAlert(message: message)
    }
    
}
