//
//  GIFImageView.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 15/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//
import Gifu
import Nuke

extension Gifu.GIFImageView {
    public override func display(image: Image?) {
        prepareForReuse()
        if let data = image?.animatedImageData {
            animate(withGIFData: data)
        } else {
            self.image = image
        }
    }
}
