//
//  UIViewController +ShowAlert.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 06/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showErrorAlert(message: String) {
        showAlert(title: "Ошибка", message: message)
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
