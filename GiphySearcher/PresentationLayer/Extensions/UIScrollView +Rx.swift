//
//  UIScrollView +Rx.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 13/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UIScrollView {
    func reachedBottom(withOffset offset: CGFloat = 0.0) -> Observable<Bool> {
        let observable = contentOffset
            .map { [weak base] contentOffset -> Bool in
                guard let scrollView = base else { return false}
                let visibleHeight = scrollView.frame.height
                    - scrollView.contentInset.top
                    - scrollView.contentInset.bottom
                let y = contentOffset.y + scrollView.contentInset.top
                let threshold = max(offset, scrollView.contentSize.height - visibleHeight)
                
                return y > threshold
        }
        
        return observable.distinctUntilChanged()
    }
    
//    func reachedBottom() -> Observable<Void>{
//        
//    }
    
}
