//
//  GPHClient +Rx.swift
//  GiphySearcher
//
//  Created by invertercant_dev on 07/01/2019.
//  Copyright © 2019 Александр Савченко. All rights reserved.
//
import GiphyCoreSDK
import RxSwift

public enum RxGPHClientError: Error {
    case unknown
}

extension Reactive where Base: GPHClient {
    
    func search(query: String,
        media: GPHMediaType = .gif,
        offset: Int = 0,
        limit: Int = 25,
        rating: GPHRatingType = .ratedR,
        lang: GPHLanguageType = .english,
        completionHandler: @escaping () -> Void) -> Observable<GPHListMediaResponse>{
        
        return Observable.create{ observer in
            
            GiphyCore.shared.search(query, media: media, offset: offset, limit: limit,rating: rating, lang: lang){ response, error in
                guard let response = response else {
                    observer.on(.error(error ?? RxGPHClientError.unknown))
                    return
                }
                observer.onNext(response)
                completionHandler()
            }
            return Disposables.create()
        }
    }
}
