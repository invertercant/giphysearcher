//
//  GiphySearcherTests.swift
//  GiphySearcherTests
//
//  Created by Александр Савченко on 17/12/2018.
//  Copyright © 2018 Александр Савченко. All rights reserved.
//

import XCTest
import RxSwift
import RxBlocking

@testable import GiphySearcher

class TestingSearchViewModel: XCTestCase {
    
    var viewModel: SearchScreenViewModel!
    var scheduler: ConcurrentDispatchQueueScheduler!
    
    override func setUp() {
        viewModel = SearchScreenViewModel()
        scheduler = ConcurrentDispatchQueueScheduler(qos: .default)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNewQuery() {
        
        let imageCollection = viewModel.imageCollectionInfo.asObservable().subscribeOn(scheduler)
        
        viewModel.queryInput.onNext("cats")
        
        let result = try! imageCollection
            .debounce(5.0, scheduler: scheduler)
            .toBlocking()
            .first()!
        
        XCTAssertEqual(viewModel.portionSize, result.count)

    }
    
    func testNewQueryAndScroll(){
        
        let expectedCount = viewModel.portionSize*2
        
        let imageCollection = viewModel.imageCollectionInfo.asObservable().subscribeOn(scheduler)
        
        viewModel.queryInput.onNext("cats")
        viewModel.scrollInput.onNext(true)
        
        let result = try! imageCollection
            .debounce(5.0, scheduler: scheduler)
            .toBlocking()
            .first()!
        
        XCTAssertEqual(expectedCount, result.count)
        
    }
    
    func testNewQueryAndFewScrolls(){
        
        let scrollsCount = Int.random(in: 2...10)
        let delay = Double(scrollsCount)*1.0
        
        let expectedCount = viewModel.portionSize*(scrollsCount+1)
        
        let imageCollection = viewModel.imageCollectionInfo.asObservable().subscribeOn(scheduler)
        
        viewModel.queryInput.onNext("cats")
        
        for _ in 1...scrollsCount{
            viewModel.scrollInput.onNext(true)
        }
        
        let result = try! imageCollection
            .debounce(delay, scheduler: scheduler)
            .toBlocking()
            .first()!
        
        XCTAssertEqual(expectedCount, result.count)
        
    }
    
    func testNewQueryAfterExistedQuery(){
        
        let expectedCount = viewModel.portionSize
        
        let imageCollection = viewModel.imageCollectionInfo.asObservable().subscribeOn(scheduler)
        
        viewModel.queryInput.onNext("cats")
        viewModel.scrollInput.onNext(true)
        viewModel.queryInput.onNext("dogs")
        
        let result = try! imageCollection
            .debounce(5.0, scheduler: scheduler)
            .toBlocking()
            .first()!
        
        XCTAssertEqual(expectedCount, result.count)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
